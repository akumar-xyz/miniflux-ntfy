# Miniflux-Ntfy

A simple process that will send push notifications for new
[Miniflux](https://miniflux.app/) entries through [ntfy](https://ntfy.sh/).

# How it works

Assuming you have already [installed the
requirements](https://pip.pypa.io/en/stable/cli/pip_install/#examples) in
`requirements.txt`

```sh
./miniflux-ntfy.py -c <path-to-config-file>
```

## Config file?

The configuration is specified in a JSON format. See
[config/config.example.json](./config/config.example.json) for an example, the
format should be simple to understand.

The config file can take multiple entries (mainly to facilitate multiple users.)

The `cron_exp` specifies how frequently the process needs to query miniflux for
new entries. The cron expression can also do second repeats using a 6th
subexpression. See [croniter documentation](
https://pypi.org/project/croniter/1.4.1/#about-second-repeats ) for more
details.


## Running using docker

### Building the image

```sh
docker build . -t miniflux-ntfy
```

### Running the image

First, place the `config.json` inside `config/`. Then,

```sh
docker run -v ./config:/config miniflux-ntfy -d
```

### Using docker compose

We can also use the docker compose `build` specification to point directly to
this repository. Below is an example `docker-compose.yaml`:

```yaml
services:
  miniflux:
    image: miniflux/miniflux:latest
    ports:
      - "80:8080"
    depends_on:
      db:
        condition: service_healthy
    environment:
      - DATABASE_URL=postgres://miniflux:secret@db/miniflux?sslmode=disable
  db:
    image: postgres:15
    environment:
      - POSTGRES_USER=miniflux
      - POSTGRES_PASSWORD=secret
    volumes:
      - miniflux-db:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "miniflux"]
      interval: 10s
      start_period: 30s
  miniflux-ntfy:
    build: https://gitlab.com/akumar-xyz/miniflux-ntfy.git
    volumes:
      - ./config:/config
    depends_on:
      - miniflux  
volumes:
  miniflux-db:
```

The contents of the `./config/config.json` might look like this:

```json
[
    {
        "cron_exp": "0 10-21/2 * * * *",
        "miniflux_instance_url" : "http://miniflux:8080/",
        "miniflux_api_token": "this_is_a_secret_super_secret_api_token1234=",
        "ntfy_details": {
            "ntfy_topic_url": "https://ntfy.example.com/news",
            "username": "phil",
            "password": "mypass"
        }
    },
    {
        "cron_exp": "0 10-21/2 * * * *",
        "miniflux_instance_url" : "http://miniflux:8080/",
        "miniflux_api_token": "this_is_a_secret_super_secret_api_token1234=",
        "ntfy_details": {
            "ntfy_topic_url": "https://ntfy.example.com/news",
            "auth_token": "tk_AgQdq7mVBoFD37zQVN29RhuMzNIz2"
        }
    },
]
```

# Features

- [x] Send Notifications
- [x] Open entry url on click
- [ ] Attach images to notification

# Contributing

Please feel free to report issues, send an MR etc

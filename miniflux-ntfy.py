#!/usr/bin/python

import asyncio
import base64
import json
import logging
import optparse
import httpx
from os.path import exists
from datetime import datetime, timezone
from urllib.parse import urlparse
from functools import reduce

from croniter import croniter
from humanize import naturaltime


def make_notifications(
    filters,
    miniflux_instance_url,
    miniflux_api_token,
):
    """
    Recieve new entries from Miniflux and return a list of 'notifications'
    """

    # Sanitize filters
    filters = {k: v for k, v in filters.items() if v}

    resp = httpx.get(
        f'{miniflux_instance_url}/v1/entries',
        params=filters,
        headers={
            "X-Auth-Token": miniflux_api_token
        },
        follow_redirects=True,
    )
    entries = resp.json()['entries']

    resp.close()

    logging.debug(entries)

    return list(map(
        lambda entry: {
            "title": entry['title'].encode(),
            "published_at": datetime.fromisoformat(entry['published_at']),
            "author": entry["author"],
            "url": entry["url"],
            "feed_name": entry["feed"]['title'],
            "icon": ("https://www.google.com/s2/favicons?"
                     f"domain={urlparse(entry['url']).netloc}&sz=32"),
        },
        entries
    )), (entries[0].get('id') if entries else None)


def send_notifications(
    notifications,
    ntfy_details,
):
    """
    Send notifications to Ntfy
    """

    ntfy_url = ntfy_details["ntfy_topic_url"]
    ntfy_auth_header = {}

    if ntfy_details.get('auth_token'):
        ntfy_auth_header = {
            "Authorization": f"Bearer {ntfy_details['auth_token']}"
        }
    elif ntfy_details.get("username") and ntfy_details.get("password"):
        creds = base64.encodebytes(
            f"{ntfy_details['username']}:{ntfy_details['password']}".encode()
        ).decode()
        ntfy_auth_header = {
            "Authorization": f"Basic {creds.strip()}"
        }

    with httpx.Client() as client:
        for notification in notifications:
            logging.debug(
                (
                    f"{notification['author']} |"
                    f"{humanize_timedelta(notification['published_at'])} |"
                    f"{notification['feed_name']}\n"
                )
            )
            resp = client.post(
                ntfy_url,
                data=(
                    f"{notification['author']} | "
                    f"{humanize_timedelta(notification['published_at'])} | "
                    f"{notification['feed_name']}\n"
                ).encode(),
                headers={
                    "Title": normalize_header(notification['title']),
                    "Tags": "newspaper_roll",
                    "Icon": notification["icon"],
                    "Click": notification["url"]
                } | ntfy_auth_header
            )
            if resp.status_code in (401, 403):
                logging.error("Ntfy: Authentication invalid!")
                return


def normalize_header(v):
    """
    Exception is thrown when headers (such as titles) have newline or return
    characters
    """
    problem_characters = ('\n', '\r')
    if not any(c.encode() in v for c in problem_characters):
        return v
    ret = reduce(
        lambda ac, ch: ac.replace(ch.encode(), " ".encode()),
        problem_characters,
        v
    )
    return ret


def humanize_timedelta(prev_time):
    now = datetime.now(timezone.utc).astimezone()
    return naturaltime(now - prev_time)


def do_notifications(
    filters,
    miniflux_instance_url,
    miniflux_api_token,
    ntfy_details
):
    """
    Create and send notifications
    """

    notifications, latest_entry_id = make_notifications(
        filters,
        miniflux_instance_url,
        miniflux_api_token,
    )
    send_notifications(notifications, ntfy_details)
    return latest_entry_id


async def loop_forever(
    cron_exp,
    miniflux_instance_url,
    miniflux_api_token,
    key,
    ntfy_details,
):
    """
    Infinite loop. Monitors how much time to sleep until next batch of
    notifications
    """

    c = croniter(cron_exp, datetime.now())

    logging.debug(
        f"{key}: initial last update : {c.get_current(datetime).isoformat()}")
    last_update_time = c.get_prev(datetime)

    logging.info(
        f"{key}: extracting notifications since : {last_update_time.isoformat()}"
        f" ({int(last_update_time.timestamp())})"
    )

    latest_entry_id = do_notifications(
        {'after': int(last_update_time.timestamp())},
        miniflux_instance_url,
        miniflux_api_token,
        ntfy_details
    )

    while True:
        last_update_time = c.get_current(datetime)
        pause_until = c.get_next(datetime)

        logging.debug(f"{key} {latest_entry_id=}")
        sleep_time = max((pause_until - datetime.now()).seconds, 0)

        logging.info(
            f"{key} will sleep till {pause_until.isoformat()}"
            f" ({sleep_time} seconds)"
        )

        await asyncio.sleep(sleep_time)

        filter = (
            {'after_entry_id': latest_entry_id}
            if latest_entry_id else
            {'after': int(last_update_time.timestamp())}
        )

        logging.info(
            f"{key}: extracting notifications using filter: {str(filter)}")

        latest_entry_id = do_notifications(
            filter,
            miniflux_instance_url,
            miniflux_api_token,
            ntfy_details
        ) or latest_entry_id


async def main(tasks):
    """
    Start async jobs
    """
    async with asyncio.TaskGroup() as tg:
        for j in tasks:
            tg.create_task(loop_forever(
                cron_exp=j['cron_exp'],
                key=j['key'],
                miniflux_instance_url=j['miniflux_instance_url'],
                miniflux_api_token=j['miniflux_api_token'],
                ntfy_details=j['ntfy_details'],
            ))


def parse_config(config_file_path):

    jobs = []
    assert exists(config_file_path), f"FILE NOT FOUND: {config_file_path}"
    with open(config_file_path) as f:
        jobs = json.load(f)

    # Validations
    assert isinstance(
        jobs, list,
    ), "Error in config. Expecting a list of objects"

    for idx, job in enumerate(jobs):
        job["key"] = job.get("key") or str(idx)

    job_validations = {
        "cron_exp": (
            lambda value: croniter.is_valid(value),
            "invalid cron expression"
        ),
        "miniflux_api_token": (
            lambda v: isinstance(v, str),
            "expecting a string for 'miniflux_api_token'"
        ),
        "miniflux_instance_url": (
            lambda v:  isinstance(v, str),
            "expecting a string for 'miniflux_instance_url'",
        ),
        "ntfy_details": (
            lambda v: isinstance(v, dict),
            "expecting a dict-like object"
        ),
        "key": (
            lambda _: True,
            ""
        ),
    }

    for job in jobs:

        missing_keys = job_validations.keys() - job.keys()
        assert not missing_keys, f"Keys {missing_keys} not found in job {job['key']}"

        for k, v in job.items():
            assert (
                k in job_validations), f"Unknown key '{k}' in job:{job['key']}"
            validator, error_msg = job_validations[k]
            assert validator(v), f"{error_msg}: {v}"

        for k in job['ntfy_details']:
            valid_keys = ('ntfy_topic_url', 'username',
                          'password', 'auth_token')
            assert k in valid_keys, f"Invalid config '{k}' is one of these: {valid_keys}"

    return jobs


if __name__ == "__main__":
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-c", "--config", dest="config_file",
                      type="string", help="(required) config file PATH")
    parser.add_option("-l", "--loglevel", dest="loglevel",
                      type="string", help="Log level")

    (options, args) = parser.parse_args()

    loglevel = getattr(logging, options.loglevel.upper()) or "warn"
    logging.basicConfig(
        format='%(asctime)s: [%(levelname)s] %(message)s', level=loglevel)


    if not options.config_file:
        logging.critical('CONFIG_FILE not specified')
        exit(-1)

    try:
        jobs = parse_config(options.config_file)
        asyncio.run(main(tasks=jobs))
    except AssertionError as e:
        logging.critical(e)
    except KeyboardInterrupt:
        logging.info("Recieved Keyboard Interrupt. Exiting...")
        exit(0)
